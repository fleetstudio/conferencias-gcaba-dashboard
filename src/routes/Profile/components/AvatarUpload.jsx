import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import Camera from '@material-ui/icons/CameraAlt';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import FileUploadDialog from '../../../components/FileUploadDialog';
import { useTranslation } from '../../../services/translation';

const useStyles = makeStyles({
  cameraIcon: {
    position: 'absolute',
    marginLeft: -75,
    marginTop: -75,
    height: 75,
    width: 150,
  },
  alignCenter: {
    textAlign: 'center',
    marginTop: 20,
  },
  avatar: {
    width: 150,
    height: 150,
  },
  avatarBackground: {
    backgroundColor: 'gray',
  },
});

export const AvatarUpload = ({ onChange, value }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const openDialog = useCallback(() => setOpen(true), [setOpen]);
  const onClose = useCallback(() => {
    setOpen(false);
  }, [setOpen]);
  const onUploadComplete = useCallback((upload) => {
    if (upload && upload.Location) onChange(upload.Location);
    setOpen(false);
  }, [setOpen, onChange]);
  return (
    <Container maxWidth="sm" className={classes.alignCenter}>
      <Grid container justify="center" alignItems="center">
        <Grid item className={classes.avatar}>
          {value && (
            <Avatar
              src={value}
              alt="avatar image"
              className={classes.avatar}
            />
          )}
          <IconButton
            className={value ? classes.cameraIcon : [classes.avatar, classes.avatarBackground].join(' ')}
            onClick={openDialog}
          >
            <Camera htmlColor="white" />
          </IconButton>
        </Grid>
      </Grid>
      <FileUploadDialog
        open={open}
        onUploadComplete={onUploadComplete}
        onClose={onClose}
        cropAspect={1}
        title={t('Upload File')}
        message={t('Select the source')}
      />
    </Container>
  );
};

AvatarUpload.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

AvatarUpload.defaultProps = {
  value: '',
};
