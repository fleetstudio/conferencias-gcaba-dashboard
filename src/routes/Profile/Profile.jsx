import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { useField, useForm } from 'react-final-form-hooks';
import { FORM_ERROR } from 'final-form';
import { useTranslation } from '../../services/translation';
import { Screen } from '../../components/Screen';
import { ScreenTitle } from '../../components/ScreenTitle';
import { AvatarUpload } from './components/AvatarUpload';
import { useGlobalContext } from '../../hooks';
import { Api } from '../../services/api';
import { getErrorMessage, makeValidator } from '../../utils/helpers';
import { makeGetErrorAndHelperText, textFieldProps } from '../../utils/materialHelpers';

const validate = makeValidator([
  { key: 'name', validators: ['required'] },
]);

export const Profile = ({ history }) => {
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const { userInfo, setIsLoading, setUserInfo } = useGlobalContext();
  const onAvatarChange = useCallback(async (file) => {
    try {
      setIsLoading(true);
      const user = await Api.updateProfile({ name: userInfo.name, avatar_url: file });
      const newUser = {
        ...userInfo,
        ...user,
      };
      setUserInfo(newUser);
      setIsLoading(false);
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      setIsLoading(false);
    }
  }, [userInfo, setIsLoading, setUserInfo, enqueueSnackbar, t]);
  const onSubmit = useCallback(async (values) => {
    try {
      setIsLoading(true);
      const user = await Api.updateProfile({ name: values.name, avatar_url: userInfo.avatar_url });
      const newUser = {
        ...userInfo,
        ...user,
      };
      setUserInfo(newUser);
      setIsLoading(false);
      history.replace('/');
      return {};
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      setIsLoading(false);
      return { [FORM_ERROR]: message };
    }
  }, [userInfo, setIsLoading, setUserInfo, enqueueSnackbar, t, history]);
  const { form, handleSubmit, submitting } = useForm({
    initialValues: userInfo,
    onSubmit,
    validate,
  });
  const name = useField('name', form);
  const getErrorAndHelperText = makeGetErrorAndHelperText(t);
  return (
    <Screen>
      <ScreenTitle title={t('Profile')} />
      {userInfo && (
        <>
          <AvatarUpload value={userInfo.avatar_url} onChange={onAvatarChange} />
          <Container maxWidth="sm">
            <form noValidate onSubmit={handleSubmit}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    {...textFieldProps(t('Name'))}
                    {...name.input}
                    {...getErrorAndHelperText(name)}
                    autoFocus
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={submitting}
              >
                {submitting
                  ? <CircularProgress size={24} />
                  : t('Save it')}
              </Button>
            </form>
          </Container>
        </>
      )}
    </Screen>
  );
};

Profile.propTypes = {
  history: PropTypes.shape().isRequired,
};

Profile.defaultProps = {};
