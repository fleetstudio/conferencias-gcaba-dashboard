import { useForm } from 'react-final-form-hooks';
import { useCallback } from 'react';
import { FORM_ERROR } from 'final-form';
import { useSnackbar } from 'notistack';
import { useHistory } from 'react-router-dom';
import { getErrorMessage, makeValidator } from '../../../utils/helpers';
import { useTranslation } from '../../../services/translation';
import { useGlobalContext } from '../../../hooks';
import { Api } from '../../../services/api';

const validate = makeValidator([
  { key: 'email', validators: ['required', 'email'] },
  { key: 'password', validators: ['required'] },
]);

export const useLoginForm = () => {
  const history = useHistory();
  const { setUserInfo, fetchSignedConferences } = useGlobalContext();
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();

  const onSubmit = useCallback(async (values) => {
    try {
      const user = await Api.login(values);
      setUserInfo(user);
      fetchSignedConferences();
      history.replace('/');
      return {};
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      return { [FORM_ERROR]: message };
    }
  }, [t, enqueueSnackbar, history, setUserInfo, fetchSignedConferences]);

  return useForm({
    initialValues: { email: '', password: '' },
    onSubmit,
    validate,
  });
};
