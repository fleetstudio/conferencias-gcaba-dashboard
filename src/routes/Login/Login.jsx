import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Screen } from '../../components/Screen';
import { LoginForm } from './components/LoginForm';

export const Login = ({ history }) => {
  const onRegisterClick = useCallback(() => history.replace('/register'), [history]);
  return (
    <Screen maxWidth="xs">
      <LoginForm onRegisterClick={onRegisterClick} />
    </Screen>
  );
};

Login.propTypes = {
  history: PropTypes.shape().isRequired,
};

Login.defaultProps = {};
