import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useField } from 'react-final-form-hooks';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useTranslation } from '../../../services/translation';
import { useRegisterForm } from '../hooks';
import { makeGetErrorAndHelperText, textFieldProps } from '../../../utils/materialHelpers';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const RegisterForm = ({ onLoginClick }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const { form, handleSubmit, submitting } = useRegisterForm(onLoginClick);
  const firstName = useField('firstName', form);
  const lastName = useField('lastName', form);
  const email = useField('email', form);
  const password = useField('password', form);
  const repeatPassword = useField('repeatPassword', form);
  const getErrorAndHelperText = makeGetErrorAndHelperText(t);
  return (
    <div className={classes.paper}>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        {t('Sign up')}
      </Typography>
      <form className={classes.form} noValidate onSubmit={handleSubmit}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <TextField
              {...textFieldProps(t('First Name'))}
              {...firstName.input}
              {...getErrorAndHelperText(firstName)}
              autoComplete="fname"
              autoFocus
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              {...textFieldProps(t('Last Name'))}
              {...lastName.input}
              {...getErrorAndHelperText(lastName)}
              autoComplete="lname"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              {...textFieldProps(t('Email'))}
              {...email.input}
              {...getErrorAndHelperText(email)}
              autoComplete="email"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              {...textFieldProps(t('Password'))}
              {...password.input}
              {...getErrorAndHelperText(password)}
              type="password"
              autoComplete="current-password"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              {...textFieldProps(t('Repeat Password'))}
              {...repeatPassword.input}
              {...getErrorAndHelperText(repeatPassword)}
              type="password"
            />
          </Grid>
        </Grid>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          disabled={submitting}
          className={classes.submit}
        >
          {submitting
            ? <CircularProgress size={24} />
            : t('Sign Up')}
        </Button>
        <Grid container justify="center">
          <Grid item>
            <Button onClick={onLoginClick}>
              {t('Already have an account? Sign in')}
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

RegisterForm.propTypes = {
  onLoginClick: PropTypes.func.isRequired,
};

RegisterForm.defaultProps = {};
