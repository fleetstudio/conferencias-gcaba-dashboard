import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Screen } from '../../components/Screen';
import { RegisterForm } from './components/RegisterForm';

export const Register = ({ history }) => {
  const onLoginClick = useCallback(() => history.replace('/login'), [history]);
  return (
    <Screen maxWidth="xs">
      <RegisterForm onLoginClick={onLoginClick} />
    </Screen>
  );
};

Register.propTypes = {
  history: PropTypes.shape().isRequired,
};

Register.defaultProps = {};
