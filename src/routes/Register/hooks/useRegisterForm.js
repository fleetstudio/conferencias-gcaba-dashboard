import { useForm } from 'react-final-form-hooks';
import { useCallback } from 'react';
import { FORM_ERROR } from 'final-form';
import { useSnackbar } from 'notistack';
import { getErrorMessage, makeValidator } from '../../../utils/helpers';
import { useTranslation } from '../../../services/translation';
import { Api } from '../../../services/api';

const samePassword = (value, form) => value !== form.password;
const validate = makeValidator([
  { key: 'firstName', validators: ['required'] },
  { key: 'lastName', validators: ['required'] },
  { key: 'email', validators: ['required', 'email'] },
  { key: 'password', validators: ['required'] },
  { key: 'repeatPassword', validators: ['required', { validator: samePassword, message: 'Passwords must match!' }] },
]);

export const useRegisterForm = (onUserComplete) => {
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();

  const onSubmit = useCallback(async (values) => {
    try {
      const registerData = {
        email: values.email,
        password: values.password,
        password_confirmation: values.repeatPassword,
        name: `${values.firstName} ${values.lastName}`,
      };
      await Api.register(registerData);
      if (onUserComplete) onUserComplete();
      return {};
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      return { [FORM_ERROR]: message };
    }
  }, [t, enqueueSnackbar, onUserComplete]);

  return useForm({
    initialValues: {
      firstName: '', lastName: '', email: '', password: '', repeatPassword: '',
    },
    onSubmit,
    validate,
  });
};
