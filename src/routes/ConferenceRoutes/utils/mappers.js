import moment from 'moment';

export const conferenceMap = (conference) => ({ ...conference, dateString: moment(conference.date).format('LLLL') });
