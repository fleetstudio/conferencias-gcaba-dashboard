import {
  useCallback, useEffect, useMemo, useState,
} from 'react';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import { Api } from '../../../services/api';
import { filterByKey, getErrorMessage, sortByKey } from '../../../utils/helpers';
import { conferenceMap } from '../utils';
import { useGlobalContext } from '../../../hooks';
import { useTranslation } from '../../../services/translation';

const sortByDate = sortByKey('date');

export const useConferences = (search) => {
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const { setIsLoading } = useGlobalContext();
  const [conferences, setConferences] = useState([]);
  const fetchConferences = useCallback(async () => {
    try {
      setIsLoading(true);
      const data = await Api.getConferences();
      if (data) {
        setConferences(data);
      }
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
    } finally {
      setIsLoading(false);
    }
  }, [setConferences, setIsLoading, enqueueSnackbar, t]);
  useEffect(() => {
    fetchConferences();
  }, [fetchConferences]);

  const parsedConferences = useMemo(
    () => conferences
      .filter((conference) => conference.enabled && moment(conference.date).isSameOrAfter(moment().startOf('day')))
      .map(conferenceMap),
    [conferences],
  );

  const filteredConferences = useMemo(
    () => parsedConferences.filter(filterByKey(search)),
    [parsedConferences, search],
  );

  const orderedConferences = useMemo(
    () => sortByDate(filteredConferences),
    [filteredConferences],
  );

  return { conferences: orderedConferences, fetchConferences };
};
