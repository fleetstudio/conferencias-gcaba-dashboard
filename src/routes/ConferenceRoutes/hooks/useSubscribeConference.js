import { useCallback, useMemo } from 'react';
import { useSnackbar } from 'notistack';
import { Api } from '../../../services/api';
import { getErrorMessage } from '../../../utils/helpers';
import { useGlobalContext } from '../../../hooks';
import { useTranslation } from '../../../services/translation';

export const useSubscribeConference = (conference, fetchConference) => {
  const { t } = useTranslation();
  const { setIsLoading, userInfo, fetchSignedConferences } = useGlobalContext();
  const { enqueueSnackbar } = useSnackbar();

  const isSignedUp = useMemo(
    () => userInfo && userInfo.signedConferences
      && userInfo.signedConferences.find((item) => +item.id === +conference.id),
    [userInfo, conference],
  );
  const onUnsubscribeClick = useCallback(async (item) => {
    try {
      setIsLoading(true);
      await Api.removeConferenceSubscription(item.id);
      await Promise.all([fetchSignedConferences(), fetchConference(item.id)]);
      setIsLoading(false);
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      setIsLoading(false);
    }
  }, [setIsLoading, enqueueSnackbar, fetchConference, fetchSignedConferences, t]);
  const onSubscribeClick = useCallback(async (item) => {
    try {
      setIsLoading(true);
      await Api.addConferenceSubscription(item.id);
      await Promise.all([fetchSignedConferences(), fetchConference(item.id)]);
      setIsLoading(false);
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      setIsLoading(false);
    }
  }, [setIsLoading, enqueueSnackbar, fetchConference, fetchSignedConferences, t]);

  return { isSignedUp, onSubscribeClick, onUnsubscribeClick };
};
