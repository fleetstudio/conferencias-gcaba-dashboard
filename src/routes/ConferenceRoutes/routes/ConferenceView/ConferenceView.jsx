import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Screen } from '../../../../components/Screen';
import { ScreenTitle } from '../../../../components/ScreenTitle';
import { useTranslation } from '../../../../services/translation';
import { useConference } from '../../hooks';
import { LeftAndRight } from '../../../../components/LeftAndRight';
import { BasicInfo } from './components/BasicInfo';
import { SignUpAction } from '../../../../components/SignUpAction';
import { UnsubscribeAction } from '../../../../components/UnsubscribeAction';
import { useSubscribeConference } from '../../hooks/useSubscribeConference';

const useStyles = makeStyles((theme) => ({
  footer: {
    position: 'fixed',
    top: 'auto',
    bottom: 0,
    left: 0,
    right: 0,
    paddingBottom: 8,
    paddingTop: 8,
    backgroundColor: theme.palette.background.default,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
  },
  screen: {
    paddingBottom: 52 + 16,
  },
}));
const footerProps = { style: { marginRight: 0 } };

export const ConferenceView = ({ match }) => {
  const { params: { id } } = match;
  const classes = useStyles();
  const { t } = useTranslation();
  const { conference, fetchConference } = useConference(id);
  const {
    isSignedUp, onSubscribeClick, onUnsubscribeClick,
  } = useSubscribeConference(conference, fetchConference);
  return (
    <Screen className={classes.screen} hideFooter>
      <ScreenTitle title={t('Conference')} />
      {!!Object.keys(conference).length && (
        <LeftAndRight
          left={<BasicInfo conference={conference} />}
          right={isSignedUp ? (
            <UnsubscribeAction conference={conference} onClick={onUnsubscribeClick} />
          ) : (
            <SignUpAction conference={conference} onClick={onSubscribeClick} />
          )}
        />
      )}
      <div className={classes.footer}>
        {isSignedUp ? (
          <UnsubscribeAction
            flipBreakpoint
            messageProps={footerProps}
            conference={conference}
            onClick={onUnsubscribeClick}
          />
        ) : (
          <SignUpAction
            flipBreakpoint
            messageProps={footerProps}
            conference={conference}
            onClick={onSubscribeClick}
          />
        )}
      </div>
    </Screen>
  );
};

ConferenceView.propTypes = {
  match: PropTypes.shape().isRequired,
};

ConferenceView.defaultProps = {};
