import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from '../../../../../services/translation';

export const BasicInfo = ({ conference }) => {
  const { t } = useTranslation();
  return (
    <>
      <Typography variant="h3">{conference.name || ''}</Typography>
      <Typography variant="subtitle1">{conference.dateString || ''}</Typography>
      <Typography
        variant="subtitle2"
        gutterBottom
      >
        {conference.available_positions ? `${t('Available positions')}: ${conference.signedUsers ? conference.signedUsers.length : 0}/${conference.available_positions}` : ''}
      </Typography>
      <Typography variant="body1">{conference.description || ''}</Typography>
    </>
  );
};

BasicInfo.propTypes = {
  conference: PropTypes.shape().isRequired,
};

BasicInfo.defaultProps = {};
