import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Screen } from '../../../../components/Screen';
import { useTranslation } from '../../../../services/translation';
import { useConferences } from '../../hooks';
import { List } from './components/List';
import { ScreenTitle } from '../../../../components/ScreenTitle';
import { Toolbar } from './components/Toolbar';

export const ConferenceList = ({ match }) => {
  const { t } = useTranslation();
  const [search, setSearch] = useState('');
  const { conferences } = useConferences(search);
  return (
    <Screen>
      <ScreenTitle title={t('Our Conferences')} />
      <Toolbar search={search} onSearchChange={setSearch} />
      <List conferences={conferences} match={match} />
    </Screen>
  );
};

ConferenceList.propTypes = {
  match: PropTypes.shape().isRequired,
};

ConferenceList.defaultProps = {};
