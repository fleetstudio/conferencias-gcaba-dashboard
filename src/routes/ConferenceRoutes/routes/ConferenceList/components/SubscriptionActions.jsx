import React from 'react';
import PropTypes from 'prop-types';
import { UnsubscribeAction } from '../../../../../components/UnsubscribeAction';
import { SignUpAction } from '../../../../../components/SignUpAction';
import { useSubscribeConference } from '../../../hooks/useSubscribeConference';
import { useConference } from '../../../hooks';

export const SubscriptionActions = ({ conference }) => {
  const { fetchConference } = useConference(conference.id, { skipAutoFetch: true });
  const {
    isSignedUp, onSubscribeClick, onUnsubscribeClick,
  } = useSubscribeConference(conference, fetchConference);
  return isSignedUp ? (
    <UnsubscribeAction conference={conference} onClick={onUnsubscribeClick} />
  ) : (
    <SignUpAction conference={conference} onClick={onSubscribeClick} />
  );
};

SubscriptionActions.propTypes = {
  conference: PropTypes.shape().isRequired,
};

SubscriptionActions.defaultProps = {};
