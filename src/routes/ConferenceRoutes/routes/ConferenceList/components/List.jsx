import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/styles';
import { useHistory } from 'react-router-dom';
import { ListBase } from '../../../../../components/ListBase';
import { SubscriptionActions } from './SubscriptionActions';
import { useGlobalContext } from '../../../../../hooks';

const renderPrimary = (item) => item.name;
const renderSecondary = (item) => item.dateString;

const useStyles = makeStyles((theme) => ({
  listItem: {
    marginBottom: 16,
    backgroundColor: theme.palette.common.white,
  },
}));

export const List = ({ conferences, match }) => {
  const { isLogged, userInfo } = useGlobalContext();
  const classes = useStyles();
  const history = useHistory();
  const onItemClick = useCallback(
    (item) => history.push(`${match.url}/${item.id}`),
    [history, match],
  );
  const renderAction = useCallback(
    (item) => isLogged && userInfo && userInfo.signedConferences && (
      <SubscriptionActions conference={item} />
    ),
    [isLogged, userInfo],
  );
  return (
    <ListBase
      onItemClick={onItemClick}
      items={conferences}
      renderPrimary={renderPrimary}
      renderSecondary={renderSecondary}
      renderAction={renderAction}
      listItemProps={{ component: Paper, className: classes.listItem }}
    />
  );
};

List.propTypes = {
  conferences: PropTypes.arrayOf(PropTypes.shape()),
  match: PropTypes.shape().isRequired,
};

List.defaultProps = {
  conferences: [],
};
