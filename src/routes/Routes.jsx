import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Login } from './Login';
import { ConferenceRoutes } from './ConferenceRoutes';
import { Profile } from './Profile';
import { Register } from './Register';

export const Routes = () => (
  <Switch>
    <Route exact path="/" render={() => <Redirect to="/conferences" />} />
    <Route path="/conferences" component={ConferenceRoutes} />
    <Route path="/profile" component={Profile} />
    <Route path="/login" component={Login} />
    <Route path="/register" component={Register} />
    <Route path="/" render={() => <Redirect to="/conferences" />} />
  </Switch>
);
