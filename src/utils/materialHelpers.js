export const makeGetErrorAndHelperText = (t = (str) => str) => (field) => {
  const helperText = field.meta.touched && field.meta.error && t(field.meta.error);
  const error = !!helperText;
  if (!error) {
    return { error };
  }
  return { error, helperText };
};

export const textFieldProps = (label) => ({
  id: label,
  label,
  fullWidth: true,
  variant: 'outlined',
  margin: 'normal',
});
