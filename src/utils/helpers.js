/* eslint-disable no-useless-escape */
import { sort } from 'ramda';
import moment from 'moment';

export const filterByKey = (query = '') => (value) => {
  const blackList = ['id'];
  if (typeof value === 'string') {
    return value.toLowerCase().includes(query.toLowerCase());
  } if (typeof value === 'number') {
    return value.toString().toLowerCase().includes(query.toLowerCase());
  } if (Array.isArray(value)) {
    return value.filter(filterByKey(query)).length > 0;
  } if (typeof value === 'object' && value !== null) {
    // eslint-disable-next-line no-restricted-syntax
    for (const [key, val] of Object.entries(value)) {
      if (!blackList.includes(key) && filterByKey(query)(val)) {
        return true;
      }
    }
    return false;
  }
  return false;
};

export const sortByKey = (key) => sort((a, b) => {
  if (typeof a[key] === 'undefined' || typeof b[key] === 'undefined') {
    return 0;
  }

  let varA = a[key];
  let varB = b[key];

  if (Number.isInteger(+varA) && Number.isInteger(+varB)) {
    varA = +varA;
    varB = +varB;
  }

  if (typeof varA === 'string') {
    varA = a[key].toUpperCase();
    varB = b[key].toUpperCase();
  }
  // eslint-disable-next-line no-nested-ternary
  return varA > varB ? 1 : varA < varB ? -1 : 0;
});

export const isEmail = (email) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const getErrorMessage = (error) => error.message;

const errorMessages = {
  required: 'Please fill out this field.',
  number: 'The value is not a valid number.',
  positive: 'The value must be a positive number',
  email: 'The value must be a valid email',
  date: 'The value must be a valid date',
};

export const makeValidator = (rules) => (values) => rules.reduce((errors, { key, validators }) => {
  const error = validators.find((validator) => {
    if (typeof validator === 'object') return validator.validator(values[key], values);
    switch (validator) {
      case 'required': {
        return Array.isArray(values[key]) ? !values[key].length : !values[key];
      }
      case 'number': {
        return Number.isNaN(+values[key]);
      }
      case 'positive': {
        return Number.isNaN(+values[key]) || +values[key] <= 0;
      }
      case 'email': {
        return !isEmail(values[key]);
      }
      case 'date': {
        return !moment(values[key]).isValid();
      }
      default:
        return false;
    }
  });
  if (error) {
    const message = typeof error === 'object' ? error.message : errorMessages[error] || error;
    return { ...errors, [key]: message };
  }
  return errors;
}, {});
