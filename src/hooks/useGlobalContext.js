import { useContext } from 'react';
import MenuDrawerContext from '../components/MenuDrawer/context';

export const useGlobalContext = () => useContext(MenuDrawerContext);
