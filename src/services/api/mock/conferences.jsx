export const CONFERENCES = [{
  id: 10, available_positions: 66, date: '2019-11-23 05:48:48', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:29', updated_at: '2019-10-29 16:57:29', description: 'Enim voluptatem optio et. Quibusdam nulla laborum libero consequuntur est.', name: 'Mariam Veum',
}, {
  id: 2, available_positions: 85, date: '2019-10-30 07:43:13', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:22', updated_at: '2019-10-29 16:57:22', description: 'A error et aut ullam voluptatem. Est voluptatem rem ut dolores quis.', name: 'Trycia Zboncak',
}, {
  id: 4, available_positions: 58, date: '2019-10-30 20:08:02', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:24', updated_at: '2019-10-29 16:57:24', description: 'Modi assumenda sequi officiis dolores. Consequatur et sint non quo eum ratione unde aut.', name: 'Jackson Lindgren',
}, {
  id: 6, available_positions: 5, date: '2019-04-14 01:48:43', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:26', updated_at: '2019-10-29 16:57:26', description: 'Voluptatem incidunt saepe deleniti sed voluptatibus at. Optio enim aut et harum dolor dolores et.', name: 'Prof. Daija Shanahan DVM',
}, {
  id: 7, available_positions: 86, date: '2019-11-27 10:48:49', enabled: false, deleted_at: null, created_at: '2019-10-29 16:57:27', updated_at: '2019-10-29 16:57:27', description: 'Natus tempore veritatis culpa. Voluptas impedit voluptatem blanditiis vel.', name: 'Dr. Kenneth Schoen I',
}, {
  id: 8, available_positions: 97, date: '2019-06-12 14:27:24', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:27', updated_at: '2019-10-29 16:57:27', description: 'Eum culpa esse tenetur exercitationem consequatur repellat. Vitae eius voluptatem est.', name: 'Joshuah Schuppe',
}, {
  id: 9, available_positions: 86, date: '2019-07-24 07:46:39', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:28', updated_at: '2019-10-29 16:57:28', description: 'Temporibus iure quod ab tenetur voluptatem voluptatem autem. Saepe aut in eum eum voluptatibus.', name: 'Neoma Dare',
}, {
  id: 11, available_positions: 39, date: '2019-03-05 05:12:15', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:29', updated_at: '2019-10-29 16:57:29', description: 'Maiores reiciendis odit repudiandae veniam facere voluptatem. Sed corporis dolorem a velit.', name: 'Ethyl Romaguera DDS',
}, {
  id: 12, available_positions: 55, date: '2019-04-15 03:31:09', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:30', updated_at: '2019-10-29 16:57:30', description: 'Odio inventore quia aut et vel. Est est sit iste corporis est id amet. Unde iure quo corrupti ut.', name: 'Dr. Rebeka Trantow IV',
}, {
  id: 13, available_positions: 7, date: '2019-06-22 19:05:16', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:30', updated_at: '2019-10-29 16:57:30', description: 'Vel vero unde illo. Rerum enim magni quia voluptate reprehenderit aut est.', name: 'Prof. Trey Cormier',
}, {
  id: 14, available_positions: 83, date: '2019-12-08 08:25:59', enabled: false, deleted_at: null, created_at: '2019-10-29 16:57:31', updated_at: '2019-10-29 16:57:31', description: 'Esse aut illum alias tenetur. Aut ratione non adipisci neque.', name: 'Princess Davis I',
}, {
  id: 15, available_positions: 43, date: '2019-03-07 20:40:13', enabled: false, deleted_at: null, created_at: '2019-10-29 16:57:31', updated_at: '2019-10-29 16:57:31', description: 'Pariatur ut beatae et et. Aut consequatur itaque ducimus quod ut magni quis.', name: 'Kaitlin Upton',
}, {
  id: 16, available_positions: 86, date: '2019-04-24 16:15:28', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:32', updated_at: '2019-10-29 16:57:32', description: 'Omnis enim rem ratione optio iste. Porro cum assumenda cum quod enim.', name: 'Prof. Simone Cummings',
}, {
  id: 17, available_positions: 40, date: '2019-12-05 02:52:59', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:32', updated_at: '2019-10-29 16:57:32', description: 'Et delectus sunt magni. Sed recusandae iusto sunt nulla. Mollitia voluptatem est dignissimos.', name: 'Cole Nienow',
}, {
  id: 18, available_positions: 65, date: '2019-05-13 17:58:17', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:33', updated_at: '2019-10-29 16:57:33', description: 'Est natus amet sunt repellat. Quibusdam odit sit velit non.', name: 'Mrs. Otilia Ebert DDS',
}, {
  id: 19, available_positions: 86, date: '2019-06-04 20:19:00', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:34', updated_at: '2019-10-29 16:57:34', description: 'Quisquam omnis qui consequuntur. Atque assumenda consequuntur exercitationem at explicabo aut.', name: 'Dr. Marielle Metz III',
}, {
  id: 20, available_positions: 10, date: '2019-07-05 05:02:45', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:34', updated_at: '2019-10-29 16:57:34', description: 'Explicabo et saepe et dolores minus. Quaerat ullam libero consequatur voluptas.', name: 'Mr. Morgan Kris DDS',
}, {
  id: 21, available_positions: 15, date: '2019-02-25 17:13:07', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:35', updated_at: '2019-10-29 16:57:35', description: 'Sit dolor consequatur velit omnis quasi sunt illo. Nihil architecto a neque vitae rerum voluptas.', name: 'Carleton Pacocha',
}, {
  id: 22, available_positions: 28, date: '2019-02-16 04:45:42', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:35', updated_at: '2019-10-29 16:57:35', description: 'Assumenda qui blanditiis vitae soluta dolorem. Quis debitis dolorum illo optio totam in.', name: 'Ms. Reva Fay V',
}, {
  id: 23, available_positions: 23, date: '2019-12-30 05:06:24', enabled: false, deleted_at: null, created_at: '2019-10-29 16:57:36', updated_at: '2019-10-29 16:57:36', description: 'Aut odit dolorum dolor recusandae. Et animi voluptas quia sed totam est id.', name: 'Torrance Upton',
}, {
  id: 24, available_positions: 85, date: '2019-07-24 15:46:59', enabled: false, deleted_at: null, created_at: '2019-10-29 16:57:37', updated_at: '2019-10-29 16:57:37', description: 'Facere ex et non rem aperiam. Id voluptatem sed fugit.', name: 'Thad Cummings V',
}, {
  id: 25, available_positions: 30, date: '2019-01-31 01:33:20', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:37', updated_at: '2019-10-29 16:57:37', description: 'Omnis est placeat aut voluptatibus. Ipsum reiciendis consequatur et voluptatem corporis.', name: 'Dr. Claudie Hills MD',
}, {
  id: 1, available_positions: 69, date: '2019-10-19 02:36:36', enabled: true, deleted_at: null, created_at: '2019-10-29 16:57:21', updated_at: '2019-10-29 16:57:21', description: 'Ipsum cupiditate inventore autem et. Libero aut inventore quisquam deleniti iure similique eaque.', name: 'Prof. Landen Quigley',
}, {
  id: 3, available_positions: 43, date: '2019-10-13 08:43:32', enabled: false, deleted_at: null, created_at: '2019-10-29 16:57:23', updated_at: '2019-10-29 16:57:23', description: 'Est optio impedit eum accusantium sint ad. Ut quaerat corrupti enim magni inventore laudantium.', name: 'Lou Kiehn',
}, {
  id: 5, available_positions: 54, date: '2019-10-01 16:28:28', enabled: false, deleted_at: null, created_at: '2019-10-29 16:57:25', updated_at: '2019-10-29 16:57:25', description: 'Repudiandae corrupti et aliquid officia et. Velit beatae doloremque excepturi ipsum dolor ratione.', name: 'Xzavier Keeling',
}];
