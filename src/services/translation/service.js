/* eslint-disable no-console */
import 'moment/locale/es';
import moment from 'moment';
import es from './files/es';

let activeFile = null;
let activeKey = 'en';
let subscriptions = [];
class TranslateService {
  static initialize() {
    const language = localStorage.getItem('language') || 'es';
    if (language) {
      TranslateService.setLanguage(language);
    }
  }

  static registerMissing(key) {
    const missingString = localStorage.getItem('language_missing');
    const missings = missingString ? JSON.parse(missingString) : {};
    if (!missings[key]) {
      // console.log('Missing translation:', key);
      missings[key] = key;
      localStorage.setItem('language_missing', JSON.stringify(missings));
    }
  }

  static t(str) {
    if (!activeFile) {
      return str;
    }
    if (!activeFile[str]) {
      TranslateService.registerMissing(str);
      return str;
    }
    return activeFile[str];
  }

  static setLanguage(key) {
    activeKey = key;
    if (key === 'es') {
      activeFile = es;
    } else {
      activeFile = null;
    }
    moment.locale(key || 'en');
    localStorage.setItem('language', key);
    subscriptions.forEach((func) => func(key));
  }

  static getLanguage() {
    return activeKey;
  }

  static subscribe(func) {
    subscriptions = [...subscriptions, func];
    return () => {
      subscriptions = subscriptions.filter((item) => item !== func);
    };
  }
}

export default TranslateService;
