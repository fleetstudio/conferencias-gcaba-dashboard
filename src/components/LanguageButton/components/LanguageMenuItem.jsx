import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import MenuItem from '@material-ui/core/MenuItem';

export const LanguageMenuItem = React.forwardRef(({
  value, label, onClick, active,
}, ref) => {
  const onMenuClick = useCallback(() => onClick(value), [onClick, value]);
  return (
    <MenuItem ref={ref} selected={active} onClick={onMenuClick}>{label}</MenuItem>
  );
});

LanguageMenuItem.propTypes = {
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  active: PropTypes.bool,
};

LanguageMenuItem.defaultProps = {
  active: false,
};
