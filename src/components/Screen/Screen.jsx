import React from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import cx from 'classnames';
import { makeStyles } from '@material-ui/styles';
import { Footer } from '../Footer';

const useStyles = makeStyles({
  defaultClass: {
    minHeight: 600,
    marginTop: 60,
  },
});

export const Screen = ({
  children, maxWidth, className, hideFooter,
}) => {
  const classes = useStyles();
  return (
    <>
      <Container maxWidth={maxWidth} className={cx(className, classes.defaultClass)}>
        {children}
      </Container>
      {!hideFooter && <Footer />}
    </>
  );
};

Screen.propTypes = {
  children: PropTypes.node.isRequired,
  maxWidth: PropTypes.string,
  className: PropTypes.string,
  hideFooter: PropTypes.bool,
};

Screen.defaultProps = {
  maxWidth: 'lg',
  className: '',
  hideFooter: false,
};
