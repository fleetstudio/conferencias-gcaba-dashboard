import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    height: '100vh',
    marginTop: -theme.mixins.toolbar.minHeight,
  },
}));

export const GridFullHeight = ({ children, ...props }) => {
  const classes = useStyles();
  return (
    <Grid container className={classes.wrapper} direction="column" justify="center" alignItems="center" {...props}>
      {children}
    </Grid>
  );
};

GridFullHeight.propTypes = {
  children: PropTypes.node,
};

GridFullHeight.defaultProps = {
  children: null,
};
