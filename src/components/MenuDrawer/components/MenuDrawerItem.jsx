import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { withRouter } from 'react-router-dom';
import { useTranslation } from '../../../services/translation';

const MenuDrawerItem = ({
  label, icon, disabled, history, path, onItemClick,
}) => {
  const { t } = useTranslation();
  const onClick = useCallback(() => {
    history.push(path);
  }, [path, history]);
  return (
    <ListItem disabled={disabled} button onClick={onItemClick || onClick}>
      <ListItemIcon>{icon}</ListItemIcon>
      <ListItemText primary={t(label)} />
    </ListItem>
  );
};

MenuDrawerItem.propTypes = {
  label: PropTypes.string.isRequired,
  history: PropTypes.shape().isRequired,
  icon: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
  path: PropTypes.string,
  onItemClick: PropTypes.func,
};

MenuDrawerItem.defaultProps = {
  path: '/',
  disabled: false,
  onItemClick: null,
};

export default withRouter(MenuDrawerItem);
