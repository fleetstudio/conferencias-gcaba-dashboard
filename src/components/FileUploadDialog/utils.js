const createImage = (url) => new Promise((resolve, reject) => {
  const image = new Image();
  image.addEventListener('load', () => resolve(image));
  image.addEventListener('error', (error) => reject(error));
  image.setAttribute('crossOrigin', 'anonymous'); // needed to avoid cross-origin issues on CodeSandbox
  image.src = url;
});
export const rotateImage = async (src, degree) => {
  const image = await createImage(src);
  const canvas = document.createElement('canvas');
  const cContext = canvas.getContext('2d');
  let cw = image.width; let ch = image.height; let cx = 0; let
    cy = 0;

  //   Calculate new canvas size and x/y coorditates for image
  // eslint-disable-next-line default-case
  switch (degree) {
    case 90:
      cw = image.height;
      ch = image.width;
      cy = image.height * (-1);
      break;
    case 180:
      cx = image.width * (-1);
      cy = image.height * (-1);
      break;
    case 270:
      cw = image.height;
      ch = image.width;
      cx = image.width * (-1);
      break;
  }

  //  Rotate image
  canvas.setAttribute('width', cw);
  canvas.setAttribute('height', ch);
  // eslint-disable-next-line no-mixed-operators
  cContext.rotate(degree * Math.PI / 180);
  cContext.drawImage(image, cx, cy);

  return new Promise((resolve) => {
    canvas.toBlob((blob) => {
      if (!blob) {
        return;
      }
      resolve(blob);
    }, 'image/jpeg');
  });
};

export const getCroppedImg = (image, crop) => {
  const canvas = document.createElement('canvas');
  const scaleX = image.naturalWidth / image.width;
  const scaleY = image.naturalHeight / image.height;
  canvas.width = crop.width;
  canvas.height = crop.height;
  const ctx = canvas.getContext('2d');

  ctx.drawImage(
    image,
    crop.x * scaleX,
    crop.y * scaleY,
    crop.width * scaleX,
    crop.height * scaleY,
    0,
    0,
    crop.width,
    crop.height,
  );

  return new Promise((resolve) => {
    canvas.toBlob((blob) => {
      if (!blob) {
        return;
      }
      resolve(blob);
    }, 'image/jpeg');
  });
};
