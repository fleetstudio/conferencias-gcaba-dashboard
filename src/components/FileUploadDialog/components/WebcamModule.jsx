import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/Camera';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import Webcam from 'react-webcam';
import { makeStyles } from '@material-ui/styles';
import { GridFullHeight } from '../../GridFullHeight/GridFullHeight';

const useStyles = makeStyles({
  pickerButton: {
    height: 64,
    width: 64,
  },
  pickerButtonIcon: {
    width: 32,
    height: 32,
  },
});

const videoConstraints = {
  width: 1280,
  height: 1280,
  facingMode: 'user',
};

function dataURItoBlob(dataURI) {
  const byteString = window.atob(dataURI.split(',')[1]);
  const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  const ab = new ArrayBuffer(byteString.length);
  const ia = new Uint8Array(ab);
  for (let i = 0; i < byteString.length; i += 1) {
    ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ab], { type: mimeString });
}

const WebcamModule = ({ onScreenshot }) => {
  const classes = useStyles();
  const [img, setImg] = useState(null);
  const [webcam, setWebcam] = useState(null);
  const restore = useCallback(() => setImg(false), [setImg]);
  const capture = useCallback(async () => {
    const imageSrc = webcam.getScreenshot();
    setImg(imageSrc);
  }, [webcam]);
  const screenshotComplete = useCallback(() => {
    const file = dataURItoBlob(img);
    if (onScreenshot) onScreenshot(file);
  }, [img, onScreenshot]);
  return (
    <GridFullHeight spacing={2}>
      <Grid item>
        <Container maxWidth="sm">
          {!img ? (
            <Webcam
              audio={false}
              height={256}
              ref={setWebcam}
              screenshotFormat="image/jpeg"
              width={320}
              videoConstraints={videoConstraints}
            />
          ) : (
            <img src={img} alt="screenshot" />
          )}
        </Container>
      </Grid>
      <Grid item>
        {!img ? (
          <Button
            className={classes.pickerButton}
            variant="outlined"
            component="span"
            aria-label="Capture"
            size="large"
            onClick={capture}
          >
            <CameraIcon className={classes.pickerButtonIcon} />
          </Button>
        ) : (
          <Grid container spacing={2}>
            <Grid item>
              <Button
                className={classes.pickerButton}
                onClick={restore}
                variant="outlined"
                component="span"
                aria-label="Take again"
                size="large"
              >
                <CloseIcon className={classes.pickerButtonIcon} />
              </Button>
            </Grid>
            <Grid item>
              <Button
                className={classes.pickerButton}
                onClick={screenshotComplete}
                variant="contained"
                component="span"
                color="primary"
                aria-label="Capture"
                size="large"
              >
                <DoneIcon className={classes.pickerButtonIcon} />
              </Button>
            </Grid>
          </Grid>
        )}
      </Grid>
    </GridFullHeight>
  );
};

WebcamModule.propTypes = {
  onScreenshot: PropTypes.func,
};

WebcamModule.defaultProps = {
  onScreenshot: null,
};

export default WebcamModule;
