import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import { GridFullHeight } from '../../GridFullHeight/GridFullHeight';

const UploadProgress = ({ file, onUpload }) => {
  const [src, setSrc] = useState(null);
  useEffect(() => {
    if (file) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setSrc(reader.result));
      reader.readAsDataURL(file);
      onUpload(file);
    }
  }, [setSrc, file, onUpload]);
  return (
    <GridFullHeight>
      <Grid item>
        {src && (
          <Container>
            <img src={src} alt="cropped" />
            <div style={{ textAlign: 'center' }}>
              <CircularProgress />
            </div>
          </Container>
        )}
      </Grid>
    </GridFullHeight>
  );
};

UploadProgress.propTypes = {
  file: PropTypes.shape().isRequired,
  onUpload: PropTypes.func.isRequired,
};

UploadProgress.defaultProps = {};

export default UploadProgress;
