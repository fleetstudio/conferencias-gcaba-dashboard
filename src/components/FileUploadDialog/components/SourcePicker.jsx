import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import CameraIcon from '@material-ui/icons/Camera';
import { makeStyles } from '@material-ui/styles';
import { GridFullHeight } from '../../GridFullHeight/GridFullHeight';

const useStyles = makeStyles({
  pickerButton: {
    height: 128,
    width: 128,
  },
  pickerButtonIcon: {
    width: 64,
    height: 64,
  },
  message: {
    position: 'absolute',
    marginTop: -95,
  },
});

const SourcePicker = ({ onFileChange, onCameraClick, message }) => {
  const classes = useStyles();
  return (
    <GridFullHeight>
      <Typography className={classes.message} align="center" variant="h3">{message}</Typography>
      <Grid item>
        <Grid container spacing={2} justify="center">
          <Grid item>
            <label htmlFor="button-file">
              <input
                accept="image/*;capture=camera"
                style={{ display: 'none' }}
                id="button-file"
                type="file"
                onChange={onFileChange}
              />
              <Button
                className={classes.pickerButton}
                variant="outlined"
                component="span"
                aria-label="Pick File"
                size="large"
              >
                <AttachFileIcon className={classes.pickerButtonIcon} />
              </Button>
            </label>
          </Grid>
          <Grid item>
            <Button
              className={classes.pickerButton}
              variant="outlined"
              component="span"
              aria-label="Camera"
              size="large"
              onClick={onCameraClick}
            >
              <CameraIcon className={classes.pickerButtonIcon} />
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </GridFullHeight>
  );
};

SourcePicker.propTypes = {
  message: PropTypes.string.isRequired,
  onFileChange: PropTypes.func.isRequired,
  onCameraClick: PropTypes.func.isRequired,
};

SourcePicker.defaultProps = {};

export default SourcePicker;
