import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/styles';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import RotateIcon from '@material-ui/icons/Rotate90DegreesCcw';
import { GridFullHeight } from '../../GridFullHeight/GridFullHeight';
import { getCroppedImg, rotateImage } from '../utils';

const useStyles = makeStyles({
  pickerButton: {
    height: 64,
    width: 64,
    marginRight: 8,
    marginLeft: 8,
  },
  pickerButtonIcon: {
    width: 32,
    height: 32,
  },
  imageWrapper: {
    maxHeight: 350,
  },
});

const CropImage = ({ file, onCropComplete, aspect }) => {
  const classes = useStyles();
  const [src, setSrc] = useState(null);
  const [cropFile, setCropFile] = useState(null);
  const [imageRef, setImageRef] = useState(null);
  const [crop, setCrop] = useState({
    aspect,
    width: 120,
    height: 120,
    x: 0,
    y: 0,
  });
  const onCropChange = useCallback((_crop) => {
    setCrop(_crop);
  }, [setCrop]);
  useEffect(() => {
    const reader = new FileReader();
    reader.addEventListener('load', () => setSrc(reader.result));
    reader.readAsDataURL(file);
  }, [file]);
  const cropCompleteHandler = useCallback(async (_crop) => {
    if (imageRef && _crop.width && _crop.height) {
      const _cropFile = await getCroppedImg(
        imageRef,
        _crop,
      );
      setCropFile(_cropFile);
    }
  }, [imageRef, setCropFile]);
  const onRotate = useCallback(async () => {
    const rotate = await rotateImage(src, 90);
    const reader = new FileReader();
    reader.addEventListener('load', () => setSrc(reader.result));
    reader.readAsDataURL(rotate);
  }, [src]);
  const onSaveClick = useCallback(() => {
    if (onCropComplete) {
      if (cropFile) onCropComplete(cropFile);
      else onCropComplete(file);
    }
  }, [onCropComplete, cropFile, file]);
  // const onRotateClick = useCallback(async () => {
  //   const rotated = await rotateImage(src, 5);
  //   setSrc(rotated);
  // }, [src, setSrc]);
  return (
    <GridFullHeight>
      {src && (
        <>
          <Grid item>
            <Container maxWidth="sm">
              <ReactCrop
                key={src}
                src={src}
                crop={crop}
                onImageLoaded={setImageRef}
                onChange={onCropChange}
                onComplete={cropCompleteHandler}
                className={classes.imageWrapper}
              />
            </Container>
          </Grid>
          <Grid item>
            <Button
              className={classes.pickerButton}
              variant="contained"
              color="primary"
              aria-label="Save"
              size="large"
              onClick={onRotate}
            >
              <RotateIcon className={classes.pickerButtonIcon} />
            </Button>
            <Button
              className={classes.pickerButton}
              variant="contained"
              color="primary"
              aria-label="Save"
              size="large"
              onClick={onSaveClick}
            >
              <SaveIcon className={classes.pickerButtonIcon} />
            </Button>
          </Grid>
        </>
      )}
    </GridFullHeight>
  );
};

CropImage.propTypes = {
  file: PropTypes.shape().isRequired,
  onCropComplete: PropTypes.func.isRequired,
  aspect: PropTypes.number,
};

CropImage.defaultProps = {
  aspect: undefined,
};

export default CropImage;
