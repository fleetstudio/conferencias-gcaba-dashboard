import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import CloseIcon from '@material-ui/icons/Close';
import S3 from 'aws-sdk/clients/s3';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import uuid from 'uuid/v4';
import { StyledAppBar as AppBar } from '../StyledAppBar';
import { Toolbar } from '../Toolbar';
import WebcamModule from './components/WebcamModule';
import SourcePicker from './components/SourcePicker';
import CropImage from './components/CropImage';
import UploadProgress from './components/UploadProgress';
import { BackButton } from '../BackButton';

function dataURItoBlob(dataURI) {
  const byteString = window.atob(dataURI.split(',')[1]);
  const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  const ab = new ArrayBuffer(byteString.length);
  const ia = new Uint8Array(ab);
  for (let i = 0; i < byteString.length; i += 1) {
    ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ab], { type: mimeString });
}

const Transition = React.forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

const FileUploadDialog = ({
  open, onClose, onUploadComplete, onError, title, message, cropAspect,
}) => {
  const [showWebcam, setShowWebcam] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const [croppedFile, setCroppedFile] = useState(null);
  const [fileToUpload, setFileToUpload] = useState(null);
  const resetState = useCallback(() => {
    setSelectedFile(null);
    setShowWebcam(false);
    setCroppedFile(null);
  }, [setSelectedFile, setShowWebcam, setCroppedFile]);
  const uploadFile = useCallback(async () => {
    try {
      if (fileToUpload) {
        const config = {
          bucketName: 'gcaba-conferencia-images',
          region: null,
          accessKeyId: process.env.REACT_APP_AWS_APP_ID,
          secretAccessKey: process.env.REACT_APP_AWS_APP_SECRET,
        };

        const S3Client = new S3(config);
        const params = {
          Bucket: 'gcaba-conferencia-images',
          Key: `${uuid()}.jpg`, // File name you want to save as in S3
          Body: fileToUpload,
        };
        const data = await new Promise((res, rej) => {
          S3Client.upload(params, (err, responseData) => {
            if (err) {
              rej(err);
            }
            res(responseData);
          });
        });
        setFileToUpload(null);
        if (onUploadComplete) onUploadComplete(data);
      }
    } catch (e) {
      console.log(e);
      if (onError) onError(e);
    }
  }, [onUploadComplete, onError, fileToUpload, setFileToUpload]);
  const onFileChange = useCallback((event) => {
    const { target: { files } } = event;
    if (files.length) {
      setSelectedFile(files[0]);
    }
  }, [setSelectedFile]);
  const onCameraClick = useCallback(() => {
    if (window.navigator && window.navigator.camera) {
      window.navigator.camera.getPicture((imageData) => {
        const dataUrl = `data:image/jpeg;base64,${imageData}`;
        const file = dataURItoBlob(dataUrl);
        setSelectedFile(file);
      }, (e) => onError(e), { destinationType: 0 });
    } else {
      setShowWebcam(true);
    }
  }, [setShowWebcam, setSelectedFile, onError]);
  const onCropComplete = useCallback((file) => {
    setFileToUpload(file);
    setCroppedFile(file);
  }, [setCroppedFile, setFileToUpload]);
  return (
    <Dialog
      fullScreen
      open={open}
      onClose={onClose}
      TransitionComponent={Transition}
      onExited={resetState}
    >
      <AppBar position="relative">
        <Toolbar
          left={!showWebcam && !selectedFile ? (
            <BackButton onBackClick={onClose} icon={<CloseIcon />} />
          ) : (
            <BackButton onBackClick={resetState} icon={<ArrowBackIcon />} />
          )}
          title={title}
        />
      </AppBar>
      {croppedFile
        ? <UploadProgress file={croppedFile} onUpload={uploadFile} />
        : selectedFile
          ? <CropImage file={selectedFile} onCropComplete={onCropComplete} aspect={cropAspect} />
          : !showWebcam
            ? (
              <SourcePicker
                message={message}
                onFileChange={onFileChange}
                onCameraClick={onCameraClick}
              />
            )
            : <WebcamModule onScreenshot={setSelectedFile} />}
    </Dialog>
  );
};

FileUploadDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onUploadComplete: PropTypes.func,
  onError: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  cropAspect: PropTypes.number,
};

FileUploadDialog.defaultProps = {
  open: false,
  onClose: () => {},
  onUploadComplete: null,
  onError: null,
  title: 'Upload File',
  message: 'Select the source',
  cropAspect: undefined,
};

export default FileUploadDialog;
