import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  title: {
    marginTop: 32,
    marginBottom: 32,
  },
});

export const ScreenTitle = ({ title }) => {
  const classes = useStyles();
  return (
    <Typography
      align="center"
      variant="h1"
      className={classes.title}
    >
      {title}
    </Typography>
  );
};

ScreenTitle.propTypes = {
  title: PropTypes.node.isRequired,
};

ScreenTitle.defaultProps = {};
