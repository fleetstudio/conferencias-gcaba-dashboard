import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useHistory } from 'react-router-dom';

export const BackButton = ({
  to, onBackClick, icon,
}) => {
  const history = useHistory();
  const onClick = useCallback(() => {
    const goBack = () => (to ? history.replace(to) : history.goBack());
    if (onBackClick) onBackClick(goBack);
    else goBack();
  }, [to, history, onBackClick]);
  return (
    <IconButton
      color="inherit"
      aria-label="Go back"
      onClick={onClick}
    >
      {icon}
    </IconButton>
  );
};

BackButton.propTypes = {
  to: PropTypes.string,
  onBackClick: PropTypes.func,
  icon: PropTypes.node,
};

const defaultIcon = <ArrowBackIcon />;
BackButton.defaultProps = {
  to: null,
  onBackClick: null,
  icon: defaultIcon,
};
