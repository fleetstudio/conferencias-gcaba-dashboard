import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

export const ListBaseItem = ({
  item,
  index,
  renderAvatar,
  primaryTypographyProps,
  secondaryTypographyProps,
  renderPrimary,
  renderSecondary,
  renderAction,
  onItemClick,
  listItemProps,
}) => {
  const onClick = useCallback(
    (event) => onItemClick(item, index, event),
    [item, index, onItemClick],
  );
  return (
    <ListItem {...listItemProps} button onClick={onClick}>
      {renderAvatar && (
        <ListItemAvatar>
          {renderAvatar(item, index)}
        </ListItemAvatar>
      )}
      <ListItemText
        primaryTypographyProps={primaryTypographyProps || { noWrap: true }}
        secondaryTypographyProps={secondaryTypographyProps}
        primary={renderPrimary(item, index)}
        secondary={renderSecondary && renderSecondary(item, index)}
      />
      {renderAction && (
        <ListItemSecondaryAction>
          {renderAction(item, index)}
        </ListItemSecondaryAction>
      )}
    </ListItem>
  );
};

ListBaseItem.propTypes = {
  renderAvatar: PropTypes.func,
  renderAction: PropTypes.func,
  renderPrimary: PropTypes.func.isRequired,
  renderSecondary: PropTypes.func,
  onItemClick: PropTypes.func,
  primaryTypographyProps: PropTypes.shape(),
  secondaryTypographyProps: PropTypes.shape(),
  item: PropTypes.oneOfType([PropTypes.shape(), PropTypes.node]).isRequired,
  index: PropTypes.number.isRequired,
  listItemProps: PropTypes.shape(),
};

const dummyFunc = () => {};
ListBaseItem.defaultProps = {
  listItemProps: {},
  renderAvatar: null,
  renderAction: null,
  renderSecondary: null,
  onItemClick: dummyFunc,
  primaryTypographyProps: null,
  secondaryTypographyProps: null,
};
