module.exports = {
  'extends': 'airbnb',
  'rules': {
    'no-underscore-dangle': 'off',
    'no-nested-ternary': 'off',
    'import/prefer-default-export': 'off',
    'react/jsx-props-no-spreading': 'off'
  },
  'globals': {
    'window': true,
    'document': true,
    'location': true,
    'localStorage': true,
    'Blob': true,
    'File': true,
    'FileReader': true,
    'Image': true
  },
  'env': {},
  'plugins': [],
};
